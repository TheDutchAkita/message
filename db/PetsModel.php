<?php

class PetsModel extends BaseModel
{
    private string $species;
    private string $breed;
    private string $fur;
    private string $tail;
    private string $name;
    private string $image;


    public function __construct()
    {
       parent::__construct();
    }

    public static function find(int $id): self
    {
        $dog = new self();

        if ($stmt = $dog->conn->prepare("select * from pets where id = :id")):
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch();

            $dog->species= $data["species"];
            $dog->breed= $data["breed"];
            $dog->name = $data["name"];
            $dog->fur = $data["fur"];
            $dog->tail = $data["tail"];
            $dog->image = $data["image"];

        endif;

        return $dog;
    }

    public function getFur(): string
    {
        return $this->fur;
    }

    public function getTail(): string
    {
        return $this->tail;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpecies(): string
    {
        return $this->species;
    }

    public function getBreed(): string
    {
        return $this->breed;
    }

    public function getImage(): string
    {
        return $this->image;
    }


}