<?php

class PersonModel extends BaseModel

{
    private string $name;
    private array $pets= [];

    public function __construct(string $name)
    {
        parent::__construct();
        $this->name = $name;

    }
    public static function find(int $id): self
    {
      $person = new self("");
      if ($stmt = $person->conn->prepare("select * from persons where id = :id")):
          $stmt->bindParam(":id", $id, PDO::PARAM_INT);
          $stmt->execute();
          $data = $stmt->fetch();

          $person->name = $data["name"];
          $person->findPets($id);
      endif;

      return $person;
    }

    public function findPets(int $id) : void
    {
       $query = "select pets.*
                    from pets
                    join person_pet on pets.id = person_pet.pet_id 
                    join persons on persons.id = person_pet.person_id
                    where persons.id = :id";

        if ($stmt = $this->conn->prepare($query)):
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            $data = $stmt->fetchAll();

            foreach ($data as $pet):
                $this->addPet(PetsModel::find($pet['id']));
            endforeach;
       endif;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addPet(PetsModel $pet) : void
    {
        $this->pets[$pet->getName()] = $pet;
    }

    /** @return PetsModel[] */
    public function getPets(): array
    {
        return $this->pets;
    }
}