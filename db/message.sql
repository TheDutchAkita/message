DROP DATABASE message;
CREATE DATABASE message;
USE message;

CREATE TABLE persons
(
id iNT NOT NULL AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100) NOT NULL
);

INSERT INTO persons (id, name) values (1, 'Matthijs');
INSERT INTO persons (id, name) values (2, 'Priscilla');
INSERT INTO persons (id, name) values (3, 'Angelina');
INSERT INTO persons (id, name) values (4, 'Jeroen');
INSERT INTO persons (id, name) values (5, 'Manuela');


CREATE TABLE pets
(
id iNT NOT NULL AUTO_INCREMENT PRIMARY KEY,
species VARCHAR(100) NOT NULL,
breed VARCHAR(100) NOT NULL,
name varchar(100) NOT NULL,
fur VARCHAR(100) NOT NULL,
tail VARCHAR(100) NOT NULL,
image VARCHAR(100) NOT NULL
);
INSERT INTO pets (id, species,breed, name, fur, tail, image)
VALUES (1, 'Hond', 'Akita', 'Cody', 'Rood', 'Krul staart', 'Cody-Priscilla.jpeg');
INSERT INTO pets (id, species, breed, name, fur, tail, image)
VALUES (2, 'Hond', 'Labrador', 'Baloe', 'Zwart', 'Otter staart', 'Baloe.jpeg');
INSERT INTO pets (id, species,breed, name, fur, tail, image)
VALUES (3, 'Poes', 'Maine Coon', 'Lottie', 'Schildpad', 'Volle staart', 'Lottie.jpeg');

CREATE TABLE person_pet
(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
person_id INT NOT NULL,
pet_id INT NOT NULL,

FOREIGN KEY (person_id) REFERENCES persons(id)
ON UPDATE CASCADE ON DELETE NO ACTION,
FOREIGN KEY (pet_id) REFERENCES pets(id)
ON UPDATE CASCADE ON DELETE NO ACTION
);

INSERT INTO person_pet(person_id, pet_id) VALUES (1,1);
INSERT INTO person_pet(person_id, pet_id) VALUES (2,1);
INSERT INTO person_pet(person_id, pet_id) VALUES (2,2);
INSERT INTO person_pet(person_id, pet_id) VALUES (3,2);
INSERT INTO person_pet(person_id, pet_id) VALUES (3,3);
INSERT INTO person_pet(person_id, pet_id) VALUES (4,2);
INSERT INTO person_pet(person_id, pet_id) VALUES (5,2);

