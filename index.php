<?php session_start(); ?>
<?php
require "./db/BaseModel.php";
require "./db/PersonModel.php";
require "./db/PetsModel.php";

$person = null;
if (!empty($_SESSION["person_id"])):
    $person = PersonModel::find($_SESSION["person_id"]);
endif;

?>
 <!doctype html>
<html lang="nl">
<head>
 <?php require "./partials/head.php"; ?>

    <title>Intake opdracht</title>
</head>
<body>
<?php require './partials/nav.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Intake opdracht</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">Dit is wat ik heb gemaakt</div>
                <div class="card-body text-center">
                    <p class="text1">
                    Dit was eigenlijk eerst al een side projectje om mijn ouders te vertellen dat ik deze opleiding wilde gaan doen.<br>
                    Wat ik heb gedaan is via een database verschillende tabellen gemaakt voor pets en person en een voor petperson.<br>
                    Ook heb ik classes gemaakt voor deze tabellen en ook een class voor basemodel.<br>
                    Dit is allemaal zelf gemaakt. Voor de style framework heb ik bootstrap gebruikt.<br>
                    </p>

                    <p class="text2">
                    Bij de inlog door gebruik van een naam komt er wat tevoorschijn.<br>
                    U krijgt een tabel te zien waarin 1 of 2 huisdieren staan die gekoppeld staan aan een naam van een persoon.<br>
                    Ook krijgt u te zien in de tabel een foto van het dier, diersoort, ras, naam, vacht kleur en soort staart. <br>
                    De volgende voornamen kunt u invullen:
                    </p>
                    <ul class="center-list">
                        <li>Angelina</li>
                        <li>Jeroen</li>
                        <li>Manuela</li>
                        <li>Matthijs</li>
                        <li>Priscilla</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($person)): ?>
        <div class="row mt-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">huisdieren van <?= $person->getName(); ?></div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Diersoort</th>
                                <th>Ras</th>
                                <th>Naam</th>
                                <th>Vacht kleur</th>
                                <th>Soort staart</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($person->getPets() as $pet):?>
                            <tr>
                                <td>
                                 <img
                                    src="/img/<?= $pet->getImage(); ?>"
                                    alt="<?= $pet->getImage(); ?>"
                                    class="img-thumbnail img-fluid"
                                    width="200px"
                                    >
                                </td>
                                <td><?= $pet->getSpecies(); ?></td>
                                <td><?= $pet->getBreed(); ?></td>
                                <td><?= $pet->getName(); ?></td>
                                <td><?= $pet->getFur(); ?></td>
                                <td><?= $pet->getTail(); ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    </div>


<?php require "./partials/footer.php"; ?>

</body>
</html>
