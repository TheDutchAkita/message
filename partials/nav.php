<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Intake opdracht</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
      </ul>

      <span class="navbar-text">
        Huidige naam: <?= $_SESSION ["username"]; ?>
      </span>

      <ul class="navbar-nav navbar-right ms-5">
        <li class="nav-item">
          <a href="/login.php" class="nav-link btn btn-outline-primary">Login</a>

        </li>
          <li class="nav-item ms-3">
              <a href="/handlers/handleLogout.php"
              class="nav-link btn btn-outline-danger">
                Logout
              </a>
          </li>
      </ul>
    </div>
  </div>
</nav>