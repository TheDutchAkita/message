<?php session_start(); ?>
<!doctype html>
<html lang="nl">
<head>
    <?php require "./partials/head.php"; ?>

    <title>Intake opdracht</title>
</head>
<body>
<?php require './partials/nav.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="h1">Login</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">login formulier</div>
                <div class="card-body">
                    <form action="/handlers/handleLogin.php" method="post">
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="username-label">Gebruikersnaam</span>
                            <input
                                    type="text"
                                    name="username"
                                    class="form-control"
                                    placeholder="Username"
                                    aria-label="Username"
                                    aria-describedby="username-label"
                            />
                        </div>
                        <div class="input-group">
                            <input type="submit" value="inloggen" class="form-control btn btn-primary"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require "./partials/footer.php"; ?>
</body>
</html>


