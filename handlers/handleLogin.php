<?php
session_start();

$pdo = new PDO(
    "mysql:host=localhost;dbname=message",
    "root",
    "root",
    [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);


$username = ucfirst(strtolower($_POST ['username']));

if ($stmt = $pdo->prepare("SELECT id FROM persons WHERE name = :name")):
 $stmt->bindParam(":name", $username, PDO::PARAM_STR);
 $stmt->execute();

 $data = $stmt->fetch();

 $_SESSION["person_id"] = $data["id"];
 $_SESSION ['username'] = $username;
endif;

header("location: /");